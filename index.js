const fse = require('fs-extra');
const path = require('path');

const { makeLogger } = require('jrz-logger');
const { Storage } = require('@google-cloud/storage');

let logger = null;
let storage;

/**
 * @description Returns a Storage instance
 */
function getStorage() {
  if (!storage) {
    storage = new Storage({
      keyFilename: process.env.GC_FILE_KEY,
    });
  }

  return storage;
}

/**
 * @description Returns a winston logger instance
 */
function getLogger() {
  if (!logger) {
    logger = makeLogger(
      process.env.GC_LOG_NAME || 'jrz-google-storage',
      process.env.GC_LOG_LEVEL || 'info'
    );
  }

  return logger;
}

/**
 * @description Faz upload do arquivo e cria os diretórios se necessário
 * @param {String} fullDirectory {folder-test/}
 * @param {Object} file {name, base64}
 */
async function upload(fullDirectory, { name, base64 }) {
  const start = Date.now();
  try {
    const buffer = Buffer.from(base64, 'base64');
    return await getStorage()
      .bucket(process.env.GC_BUCKT_NAME)
      .file(`${fullDirectory}/${name}`)
      .save(buffer);
  } catch (e) {
    getLogger().error(`Upload error -> ${e.message}`);
    throw e;
  } finally {
    getLogger().debug(`Upload completed: (${Date.now() - start}ms)`);
  }
}

/**
 * @description Faz download do arquivo
 *
 * @param {String} fullDirectory
 * @param {String} encoding (base64, utf8)
 */
async function download(fullDirectory, encoding) {
  const start = Date.now();
  let fullPath = null;

  try {
    const tmpDir = './tmp/download';
    const filename = fullDirectory
      .split('/')
      .filter((w) => w !== '')
      .join('-');

    fullPath = path.resolve(`${tmpDir}/${filename}`);

    if (!fse.existsSync(tmpDir)) {
      fse.mkdirsSync(path.resolve(tmpDir));
    }

    await getStorage()
      .bucket(process.env.GC_BUCKT_NAME)
      .file(fullDirectory)
      .download({ destination: fullPath });

    return fse.readFileSync(fullPath, { encoding });
  } catch (e) {
    getLogger().error(`Downdload error -> ${e.message}`);
    throw e;
  } finally {
    await fse.unlinkSync(fullPath);
    getLogger().debug(`Downdload completed: (${Date.now() - start}ms)`);
  }
}

async function copyFile(filePath, newLocation) {
  const start = Date.now();

  try {
    const file = await getStorage()
      .bucket(process.env.GC_BUCKT_NAME)
      .file(filePath);

    const response = await file.copy(newLocation);

    return response;
  } catch (e) {
    getLogger().error(`Copy error -> ${e.message}`);
    throw e;
  } finally {
    getLogger().debug(`Copy completed: (${Date.now() - start}ms)`);
  }
}

async function deleteFiles(filePath) {
  const start = Date.now();
  try {
    return getStorage()
      .bucket(process.env.GC_BUCKT_NAME)
      .deleteFiles({ prefix: filePath });
  } catch (e) {
    console.log(e);
    getLogger().error(`Delete error -> ${e.message}`);
    throw e;
  } finally {
    getLogger().debug(`Delete completed: (${Date.now() - start}ms)`);
  }
}

async function getFiles(filePath) {
  const start = Date.now();

  try {
    return getStorage()
      .bucket(process.env.GC_BUCKT_NAME)
      .getFiles({ prefix: filePath });
  } catch (e) {
    console.log(e);
    getLogger().error(`Get error -> ${e.message}`);
    throw e;
  } finally {
    getLogger().debug(`Get completed: (${Date.now() - start}ms)`);
  }
}

module.exports = {
  upload,
  download,
  copyFile,
  deleteFiles,
  getFiles,
};
