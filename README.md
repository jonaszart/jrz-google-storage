# jrz-google-storage

Biblioteca que disponibiliza funções para upload e download de arquivos da GC.

## Instalação

```
npm install jrz-google-storage
```

## Configurações

As seguintes variaveis de ambiente devem estar disponiveis para um funcionamento correto da lib.

#### Obrigatórias

- GC_FILE_KEY - Deve conter o endereço do arquivo .json com as configurações para conexão com o google-cloud
- GC_BUCKT_NAME - Nome do bucket onde os se deseja salvar os arquivos

#### Opcionais
- GC_LOG_NAME - Label utilizado para criação de um instância de jrz-logger, se nenhum valor for informado o valor utilizado é jrz-google-storage
- GC_LOG_LEVEL - Level de log que deve ser utilizado pelo lib, se nenhum valor for informado o valor utilizado é info