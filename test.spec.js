const assert = require('assert');
const hlps = require('jrz-helpers');
const gcs = require('./index');

process.env.NODE_ENV = 'test';
process.env.LOG_LEVEL = 'info';
process.env.GC_FILE_KEY = './comac-326615-c9ae5dd993da.json';
process.env.GC_BUCKT_NAME = 'comac_files_localhost';

describe('', async () => {
  it('should upload image and delete', async () => {
    const base64 = hlps.randomBase64Image();
    const name = `${hlps.randomUuid()}.${hlps.getBase64Extension(base64)}`;
    console.log(name);
    const upload = await gcs.upload('package-test', {
      name,
      base64,
    });
    assert.equal(upload, undefined);

    const deleteFiles = await gcs.deleteFiles('package-test');
    assert.equal(deleteFiles, []);
  });
});
